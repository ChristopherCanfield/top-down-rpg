# Top-Down RPG Template #

An ActionScript 3 template for prototyping top-down RPG style games. Uses the Flixel game framework.

[Try it out.]()

![top-down-rpg.jpg](https://bitbucket.org/repo/nbg4zE/images/587029322-top-down-rpg.jpg)

Christopher D. Canfield  
September 2014