package com.divergentthoughtsgames.assets 
{
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class Audio 
	{
		// ==== Music ====
		
		
		// ==== Sound Effects ====
		
		[Embed(source = '../../../../res/sound/ocean-waves.mp3')]
		public const OceanWaves: Class;
		
		[Embed(source = '../../../../res/sound/ambient-outdoor-noise.mp3')]
		public const AmbientOutdoorNoise: Class;
		
		[Embed(source = '../../../../res/sound/church-bells.mp3')]
		public const ChurchBells: Class;
	}
}