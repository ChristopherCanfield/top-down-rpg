package com.divergentthoughtsgames 
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxRect;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxTilemap;
	import org.flixel.FlxG;
	import org.flixel.plugin.photonstorm.FlxBar;
	import org.flixel.plugin.photonstorm.FlxWeapon;
	import org.flixel.plugin.photonstorm.FlxMath;
	
	import com.divergentthoughtsgames.gamestate.PlayState;
	import com.divergentthoughtsgames.assets.Assets;
	
	/**
	 * The player.
	 * @author Christopher D. Canfield
	 */
	public class Player extends FlxSprite
	{		
		private var startX:int;
		private var startY:int;
		
		private var level:FlxTilemap;
		private var gameState:FlxState;
		
		private var lives:int = 3;
		
		public function Player(gameState:FlxState, level:FlxTilemap, startX:int, startY:int) 
		{
			this.startX = startX;
			this.startY = startY;
			
			this.level = level;
			this.gameState = gameState;
			
			x = startX;
			y = startY;
			maxVelocity.x = maxVelocity.y = 80;
			drag.x = maxVelocity.x * 4;
			drag.y = maxVelocity.y * 4;
			
			var image:FlxSprite = loadGraphic(Assets.graphics.Chickens, true, true, 16, 19);
			addAnimation("walk", [0, 1, 2], 12, true);
			addAnimation("stop", [0], 0);
			play("stop");
		}
			
		public function getLives(): int
		{
			return lives;
		}
	
		override public function update():void
		{
			if (checkForDeath())
			{
				lives--;
				resetAtStartPosition();
			}
			
			processUserInput();
		}
		
		private function processUserInput():void
		{
			acceleration.x = 0;
			acceleration.y = 0;
			
			var minX:Number = 0;
			var minY:Number = 0;
			var maxX:Number = level.width - frameWidth;
			var maxY:Number = level.height - frameHeight;
			
			if ((FlxG.keys.LEFT || FlxG.keys.A) && (x > minX))
			{
				// The player reaches its top speed very quickly.
				acceleration.x = -maxVelocity.x * 4;
				facing = FlxObject.LEFT;
			}
			else if ((FlxG.keys.RIGHT || FlxG.keys.D) && (x < maxX))
			{
				acceleration.x = maxVelocity.x * 4;
				facing = FlxObject.RIGHT;
			}
			
			if ((FlxG.keys.UP || FlxG.keys.W) && (y < maxY))
			{
				acceleration.y = -maxVelocity.y * 4;
				facing = FlxObject.UP;
			}
			else if (FlxG.keys.DOWN || FlxG.keys.S && (y > minY))
			{
				acceleration.y = maxVelocity.y * 4;
				facing = FlxObject.DOWN;
			}
			
			stayWithinBounds(minX, minY, maxX, maxY);
			
			if (FlxG.keys.CONTROL)
			{
				trace("X: " + x + "; Y: " + y);
			}
			
			animate();
		}
		
		private function stayWithinBounds(minX: Number, minY: Number, maxX: Number, maxY: Number): void
		{
			if (x > maxX)
			{
				x = maxX - 1;
				acceleration.x = velocity.x = 0;
			}
			else if (x < minX)
			{
				x = minX + 1;
				acceleration.x = velocity.x = 0;
			}
			
			if (y > maxY)
			{
				y = maxY - 1;
				acceleration.y = velocity.y = 0;
			}
			else if (y < minY)
			{
				y = minY + 1;
				acceleration.y = velocity.y = 0;
			}
		}
		
		override public function reset(x: Number, y: Number): void
		{
			super.reset(x, y);
			
			this.acceleration.x = 0;
			this.acceleration.y = 0;
		}
		
		/**
		 * Resets the player at the starting position.
		 */
		private function resetAtStartPosition():void
		{
			reset(startX, startY);
		}
		
		private function checkForDeath():Boolean
		{
			return (y > level.height);
		}
		
		private function animate() : void
		{
			if (velocity.x != 0 || velocity.y != 0) 
			{
				play("walk");
			} 
			else 
			{ 
				play("stop");
			}
		}
	}
}